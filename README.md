# Workouts

[Beginner](#beginner)

[Strength](#strength)

[Hypertrophy](#hypertrophy)

[Endurance](#endurance)

[Circuits](#circuits)

[General No Goal Tracey Workout](#general-no-goal-tracey-workout)

   1. [Chest and Shoulders](#chest-and-shoulders)
   2. [Legs](#legs)
   3. [Arms](#arms)
   4. [Back](#back)
   5. [Cardiabs](#cardiabs)

[Shapely Leg and Glute / Stay at Home Workout](#shapely-leg-and-glute--stay-at-home-workouts)

   1. [General Overall Legs](#general-overall-legs)
   2. [Cardiabs 2](#cardiabs-2)
   3. [Upper Body](#upper-body)
   4. [Glute Shape Builder Cycle 1](#glute-shape-builder-cycle-1)
   5. [Glute Shape Builder Cycle 2](#glute-shape-builder-cycle-2)

[Explanation Encyclopedia](#explanation-encyclopedia)

## Beginner

**Goal**: Get your body used to working out while focusing on form and using low weights. Ideally you would use this workout for a month or two before increasing the difficulty to allow your body time to adjust.

**Algorithm**: 6 Exercises

**Time Between Sets**: Like 30 secs to 1 minute, however much you need really

**Workout Days**: Push, Pull, Legs, Cardio

**Schedule**: 4 active days, 3 rest days

**Time to Complete**: 30 minutes to 45 minutes

**Additional Notes**: @TODO

---

## Strength

**Goal**: @TODO

**Algorithm**: @TODO

**Time Between Sets**: @TODO

**Workout Days**: @TODO

**Schedule**: @TODO

**Time to Complete**: @TODO

**Additional Notes**: @TODO

---

## Hypertrophy

**Goal**: @TODO

**Algorithm**: @TODO

**Time Between Sets**: @TODO

**Workout Days**: @TODO

**Schedule**: @TODO

**Time to Complete**: @TODO

**Additional Notes**: @TODO

---

## Endurance

**Goal**: @TODO

**Algorithm**: @TODO

**Time Between Sets**: @TODO

**Workout Days**: @TODO

**Schedule**: @TODO

**Time to Complete**: @TODO

**Additional Notes**: @TODO

---

## Circuits

**Goal**: Full body, endurance training for use as a group activity.

**Algorithm**: 2 sets of circuits with 45 seconds on and 15 seconds off

**Time Between Sets**: 15 seconds

**Workout Days**: 1

**Schedule**: No

**Time to Complete**: 30-45 min

**Additional Notes**:

---

### Full Body
| Exercise               | Sets    |
| ---------------------- | :------ |
| Burpies                | 2x45sec |
| Back Flys              | 2x45sec |
| Goblet Squats          | 2x45sec |
| Plate Chest Press      | 2x45sec |
| Planks                 | 2x45sec |
| Hammer Curls           | 2x45sec |
| Tricep Extensions      | 2x45sec |
| Bent Over Rows (Plate) | 2x45sec |

| Exercise              | Sets    |
| --------------------- | :------ |
| Medicine Ball Tosses  | 2x45sec |
| Resistance Band Curls | 2x45sec |
| Pushups               | 2x45sec |
| Lunges                | 2x45sec |
| Russian Twists        | 2x45sec |
| Plate Overhead Press  | 2x45sec |
| Squat Kicks           | 2x45sec |
| T-Poses               | 2x45sec |

---

## General No Goal Tracey Workout

**Goal**: These are the workouts I did in college. Idk man we just out here working out man. I basically stayed fit, and fairly low body fat with these workouts. I basically tried to do a variety of different exercises with different muscle groups. Didn't gain too much mass overall with these, however that could have been attributed to diet, sleep, and stress. YMMV.

**Algorithm**: 6-8 Exercises, 3 sets of 10 reps

**Time Between Sets**: Like 30 secs to 1 minute, however much you need really

**Workout Days**: Chest and Shoulders, Legs, Arms, Back, Cardiabs

**Schedule**: 5 Active Days, 2 Rest Days

**Time to Complete**: 45 minutes - 1 hour

**Additional Notes**:

- Try to keep it difficult such that you can do the first set fine, but sets 2 or 3 are difficult to complete all 10 reps in.
- The first number is the sets, second number is the reps, and last number is the weight (it'll be different depending on the person of course)
- For your workouts copy the workouts and put the weight you last did after the @ so you can remember it. ie 3x10@135 Bench Press
- Additional explanations and notes are in the Explanation Encyclopedia for exercises with an *
- Try not to do Legs or Chest after Back days. Also try not to do Cardiabs after Leg days. Lastly try not to do Chest after Arm days, Triceps are important here.
- If you're getting through the workout easily try adding some of the optional ones in there or throwing in both exercises for an OR.

---

### Chest and Shoulders

| Exercise                   | Sets     | Alt Ex. Index |
| -------------------------- | :------- | :-----------: |
| Bench Press                | 3x10@xxx |       A       |
| Overhead BB Shoulder Press | 3x10@xxx |       B       |
| Incline Chest Press        | 3x10@xxx |       -       |
| Decline Chest Press        | 3x10@xxx |       -       |
| Machine Overhead Press     | 3x10@xxx |       -       |
| Neutral Pec Flys           | 4x10@xxx |    C && D     |
| Shoulder Raise Machine     | 3x10@xxx |       E       |

| Alternate Exercises | Alt Sets | Index |
| ------------------- | :------- | ----- |
| Machine Chest Press | 3x10@xxx | A     |
| Overhead DB Press   | 3x10@xxx | B     |
| Wolverines*         | 2x10@xxx | C     |
| Bearhugs*           | 2x10@xxx | D     |
| T-Poses*            | 3x8@xxx  | E     |

---

### Legs

| Exercise                     | Sets     | Alt Ex. Index |
| ---------------------------- | :------- | :-----------: |
| Squats                       | 3x10@xxx |       A       |
| Lunges (Body Weight)         | 3x16@xxx |       -       |
| Goblet Squats                | 3x10@xxx |    B OR C     |
| Leg Extensions               | 3x10@xxx |       -       |
| Leg Curls                    | 3x10@xxx |       -       |
| Pelvis Thrusts/Glute Bridge  | 3x10@xxx |       D       |
| Abductor Machine (Super Set) | 2x10@xxx |       -       |
| Adductor Machine (Super Set) | 2x10@xxx |       -       |
| Calf Raises                  | 3x10@xxx |       -       |

| Alternate Exercises | Alt Sets | Index |
| ------------------- | :------- | ----- |
| Leg Press Machine   | 3x10@xxx | A     |
| Jump Squats         | 3x10@xxx | B     |
| Box Jumps           | 3x10@xxx | C     |
| Glute Machine       | 3x10@xxx | D     |

---

### Arms

| Exercise                            | Sets     | Alt Ex. Index |
| ----------------------------------- | :------- | :-----------: |
| Dumbbell Bicep Curls                | 3x10@xxx |       -       |
| Tricep Pulldowns                    | 3x10@xxx |       -       |
| Machine Curls                       | 3x10@xxx |       -       |
| Dumbbell Tricep Extensions          | 3x10@xxx |       A       |
| Hammer Curls                        | 3x10@xxx |       -       |
| 1-Hand Cable Curls                  | 3x10@xxx |       -       |
| 1-Hand Cable Tricep Extensions      | 3x10@xxx |       -       |
| Forearm Curls                       | 3x10@xxx |       -       |
| Machine Curl Countdowns* (Optional) | 2x2@xxx  |       B       |

| Alternate Exercises                    | Alt Sets | Index |
| -------------------------------------- | :------- | :---: |
| Diamond Tricep Extensions              | 3x10@xxx |   A   |
| Bicep Cable Curls (Run The Stack Var)* | ?x10,?x5 |   B   |

---

### Back

| Exercise                      | Sets      | Alt Ex. Index |
| ----------------------------- | :-------- | :-----------: |
| Row Machine (Optional Warmup) | 2min@500m |       -       |
| Pullups (Unassisted)          | 4xAMAP    |       A       |
| Dumbbell Rows                 | 3x10@xxx  |       B       |
| Lat Pulldowns                 | 3x10@xxx  |       -       |
| Wide Grip Seated Row          | 3x10@xxx  |       -       |
| Delt Fly Machine              | 3x10@xxx  |       C       |
| Shrugs                        | 3x10@xxx  |       -       |

| Alternate Exercises   | Alt Sets | Index |
| --------------------- | :------- | ----- |
| Pullups (Assisted)    | 3x10@xxx | A     |
| Close Grip Seated Row | 3x10@xxx | B     |
| Delt Fly Dumbbells    | 3x10@xxx | C     |

---

### Cardiabs

| Exercise                 | Sets     | Alt Ex. Index |
| ------------------------ | :------- | :-----------: |
| Frogs (Circuit)          | 2x20     |       -       |
| Reverse Planks (Circuit) | 2x30sec  |       -       |
| Mason Twists (Circuit)   | 2x20     |       -       |
| Planks (Circuit)         | 2x30sec  |       -       |
| Ab Leg Extensions        | 3x10@xxx |    A OR B     |
| Ab Machine               | 3x10@xxx |    C && D     |
| 1 Mile Run               | 7-9mph   |       E       |

| Alternate Exercises                 | Alt Sets | Index |
| ----------------------------------- | :------- | ----- |
| Ab Leg Extensions Side to Side Var* | 3x10@xxx | A     |
| Flutter Kicks                       | 3x30sec  | B     |
| Heel Touches*                       | 2x10@xxx | C     |
| Up and Overs*                       | 2x10     | D     |
| 1 Mile Walk                         | 3.5-4mph | E     |

---

## Shapely Leg and Glute / Stay at Home Workouts

**Goal**: Goal here is to train through cardio, build and shape your legs and glutes, bring up your endurance, and lose weight. These workouts were intended for at home without a gym. Additionally the upper body workout is there to make sure you don't look like a noodle (but is less effective since this whole workout is designed around not having a gym).

**Algorithm**: Walk/run with 6-8 Exercises, 3 sets of 12-20 reps, endurance training so go for more reps but not more than 20

**Time Between Sets**: Like 10 secs to 30 sec, less is better

**Workout Days**: Choose your poison each day depending on what you need to focus on, or just cycle through the workouts like so;

**Schedule**: 5 active days, 2 active rest days. Active rest days just do at least a 15 minute walk (not run)

**Time to Complete**: 45 minutes - 90 minutes

**Additional Notes**:

- Each day you want to at the very least walk/run 15-30 minutes. To start out just walk at a consistent fast pace, and decide to jog after a week or two keeping a consistent pace.
- Note on the Glute Shaper workouts, each one should take around 10 minutes, so with that being said you should do one of those workouts twice or do both of them (both is what I recommend) along with the 15-30 minute walk or run.
- You can use resistance bands on the Glute Shaper workouts to increase the difficulty but start without to begin for the first week or two.
- For the Upper Body workouts, you might have to get creative to get some kind of weight/resistance. For example fill a backpack with books and use that for curls/shrugs/overhead press, use two filled water bottles for t-poses, use a guitar case with your guitar in it for rows. Alternatively just buy some cheap dumbbells (I suggest a pair of 10s and a pair of 20, 25, or 30)
- Yoga mat is recommended for the glute exercises and ab workouts. Resistance bands recommended after a few weeks. And Dumbbells are recommended if you can find some good cheap ones.
- For the Glute Shape Builder 2 workouts, for ones where you need to do each leg individually, count how many you do for the first side under that time and make sure you do that many on the other side.

---

### General Overall Legs
| Exercise           | Sets      | Alt Ex. Index |
| ------------------ | :-------- | :-----------: |
| Walk / Run         | 15-30min  |       -       |
| Stairs             | x8flights |       A       |
| Body Weight Squats | 3x20      |       -       |
| Calf Raises        | 3x20@xxx  |       -       |
| Crab Walks         | 3x10@xxx  |       -       |
| Donkey Kicks       | 3x20@xxx  |       -       |
| Fire Hydrants      | 3x20@xxx  |       -       |
| Side Leg Raises    | 3x20@xxx  |       -       |
| Thrusts & Abductor | 3x20@xxx  |       -       |

| Alternate Exercises | Alt Sets | Index |
| ------------------- | :------- | ----- |
| Lunges              | 3x20@xxx | A     |

---

### Cardiabs 2
| Exercise                 | Sets     |
| ------------------------ | :------- |
| Walk / Run               | 15-30min |
| Frogs (Circuit)          | 2x20     |
| Reverse Planks (Circuit) | 2x30sec  |
| Mason Twists (Circuit)   | 2x20     |
| Planks (Circuit)         | 2x30sec  |
| Flutter Kicks            | 3x30sec  |
| Heel Touches*            | 2x10     |
| Up and Overs*            | 2x10     |

---

### Upper Body
| Exercise       | Sets        |
| -------------- | :---------- |
| Walk / Run     | 15-30min    |
| Bicep Curl     | 3x20-20@xxx |
| Pushups        | 3x20        |
| Rows           | 3x16-16@xxx |
| Tricep Dips    | 3x16        |
| T-Poses        | 3x10@xxx    |
| Shrugs         | 3x20@xxx    |
| Overhead Press | 3x20@xxx    |

---

### Glute Shape Builder Cycle 1
| Exercise                         | Sets    |
| -------------------------------- | :------ |
| Fire Hydrant                     | 1x12    |
| Straight Leg Fire Hydrant        | 1x12-12 |
| Donkey Kicks                     | 1x12-12 |
| Side Lying Hip Abduction         | 1x12-12 |
| Wiggly Wobbly's (Abductor Squat) | 30sec   |
| Squat Abduction Lift             | 30sec   |
| Side Lunges                      | 30sec   |
| Side Curtsy Lunges               | 30sec   |
| Sumo Squat                       | 30sec   |
| Standing Leg Raises              | 1x12-12 |
| Ins and Outs (Full Clamshells)   | 1x12-12 |
| Quadruped Hip Circle             | 1x12-12 |
| Glute Bridge w/ Abduction        | 30sec   |
| Side Shuffle                     | 30sec   |

[See Also](https://www.youtube.com/watch?v=rqCKZtz23Mk)

---

### Glute Shape Builder Cycle 2
| Exercise                           | Sets        |
| ---------------------------------- | :---------- |
| Side Clam                          | 40sec-40sec |
| Fire Hydrant Kick                  | 40sec-40sec |
| 3/4 Leg Circle Extension           | 40sec-40sec |
| Rainbow                            | 40sec-40sec |
| Glute Bridge Abduction             | 40sec       |
| Frog Pump                          | 40sec       |
| Leg Circles (20sec clock, counter) | 40sec-40sec |
| Glute Tap                          | 40sec       |

[See Also](https://www.youtube.com/watch?v=umUtH9gZIfc)

---

## Explanation Encyclopedia

1. AMAP - As many as possible
2. Wolverines -  cables set to the ground pulling up and inward like wolverine X motion
3. Bearhugs - cables set to top pulling down and inward basically aggressively hugging
4. T-Poses - low weight dumbbells, stand up straight bring the dumbbells from your side, out to a t-pose, then for extra pain bring your arms in front of you, and down in front of you, then back up, back around to the t-pose, and back down to your sides. That's 1 rep
5. Bicep Cable Curls (Run The Stack Variation) - Use a straight bar, cables, lowest setting. Start out at 5 pounds, do 10 reps, after finishing the set immediately go up 5 more pounds and do it again, keep going until you cant do all 10 reps. Then start going down, removing 5 pounds each time, and doing 5 reps instead of 10. There should be no rest at all during this whole sequence and should exhaust you (hence doing this at the end of the workout).
6. Machine Curl Countdowns - You will need another person for this one. Set the weight to something you cannot curl. Have the other person help you lift the machine to the top, at the top they will let go gently and you will try to let it down as slowly as possible. Do this twice and then give yourself a long rest before the second set.
7. Ab Leg Extensions Side to Side Variation - Put a roller in front of the ab machine, and put your legs to one side. Lift your legs up and over the roller to the other side, and repeat.
8. Heel Touches - Lay on your back, put you legs up at a 90 degree angle. Using your abs, reach upward to try and touch your heels.
9. Up and Overs - Lay on your back, spread your legs apart. Reach up and over to the opposite leg to try and touch your toes. Do this for each side and repeat.

---
